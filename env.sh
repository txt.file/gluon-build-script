#!/bin/bash

# build date + gluon release info
GLUON_RELEASE=$(date '+%Y%m%d')"+"
if [ "HEAD" = "$(git rev-parse --abbrev-ref HEAD)" ]; then
        GLUON_RELEASE="$GLUON_RELEASE$(git describe --tags)"
else
        GLUON_RELEASE="$GLUON_RELEASE$(git rev-parse --abbrev-ref HEAD)-$(git rev-parse --short --verify HEAD)"
fi

# gluon targets
STABLE_TARGETS="$(make 2>/dev/null | grep "^ * " | grep -o "[[:alnum:]]\\+-\\?[[:alnum:]]*")"
ALL_TARGETS="$(make BROKEN=1 2>/dev/null | grep "^ * " | grep -o "[[:alnum:]]\\+-\\?[[:alnum:]]*")"

# export everything
export GLUON_RELEASE
export STABLE_TARGETS
export ALL_TARGETS
