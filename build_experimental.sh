#!/bin/bash

# This is a build script to run make for all the supported GLUON_TARGETS and our gluon site configurations.
# Copyright (C) 2017,2019 Vieno Hakkerinen
# SPDX-License-Identifier: MIT

. "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/env.sh"

GLUON_BRANCH="experimental"

export GLUON_BRANCH

rm output/images/factory/* output/images/sysupgrade/*
for TARGET in $ALL_TARGETS; do
	if ! make -j"$(nproc || echo -n 1)" GLUON_TARGET="$TARGET" BROKEN=1; then
		echo "$(date -Isecond) could not build $TARGET for multidomain" >> build.messages
		exit
	fi
done
make manifest GLUON_GLUON_BRANCH="$GLUON_BRANCH" GLUON_PRIORITY=1 BROKEN=1
rm /var/www/firmware/multidomain/"$GLUON_BRANCH"/factory/* /var/www/firmware/multidomain/"$GLUON_BRANCH"/sysupgrade/*
mkdir -p /var/www/firmware/multidomain/"$GLUON_BRANCH"/ || touch -m /var/www/firmware/multidomain/"$GLUON_BRANCH"/
cp -r output/images/* /var/www/firmware/multidomain/"$GLUON_BRANCH"/
./contrib/sign.sh /home/buildbot/keys/key.secret /var/www/firmware/multidomain/"$GLUON_BRANCH"/sysupgrade/"$GLUON_BRANCH".manifest
